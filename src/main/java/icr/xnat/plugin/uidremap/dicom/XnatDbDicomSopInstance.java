/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.xnat.plugin.uidremap.dicom;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.SopInstance;
import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import org.dcm4che2.data.DicomObject;

/**
 *
 * @author jamesd
 */
final class XnatDbDicomSopInstance extends AbstractDisplayable
	implements SopInstance
{
	private File file;
	private String uid = "";

	XnatDbDicomSopInstance(String uid, String filename)
	{
		this.uid = uid;
		file = new File(filename);
	}

	@Override
	public void compact()
	{}
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"File: "+file.getAbsolutePath());
		ps.println(pad+"Uid: "+uid);
	}

	@Override
	public String getAcquisitionTime()
	{
		return "";
	}

	@Override
	public int getColumnCount()
	{
		return 0;
	}

	@Override
	public String getContentTime()
	{
		return "";
	}

	@Override
	public DicomObject getDicomObject()
	{
		return null;
	}

	@Override
	public File getFile()
	{
		return file;
	}

	@Override
	public String getFrameOfReferenceUid()
	{
		return "";
	}

	@Override
	public double[] getImageOrientationPatient()
	{
		return new double[] {Double.NaN,Double.NaN,Double.NaN,Double.NaN,
			Double.NaN,Double.NaN};
	}

	@Override
	public double[] getImageOrientationPatient(int i)
	{
		return new double[] {Double.NaN,Double.NaN,Double.NaN,Double.NaN,
			Double.NaN,Double.NaN};
	}

	@Override
	public double[] getImagePositionPatient()
	{
		return new double[] {Double.NaN,Double.NaN,Double.NaN};
	}

	@Override
	public double[] getImagePositionPatient(int i)
	{
		return new double[] {Double.NaN,Double.NaN,Double.NaN};
	}

	@Override
	public int getInstanceNumber()
	{
		return 1;
	}

	@Override
	public String getModality()
	{
		return "";
	}

	@Override
	public int getNumberOfFrames()
	{
		return 0;
	}

	@Override
	public String getPath()
	{
		return file.getPath();
	}

	@Override
	public double[] getPixelSpacing()
	{
		return new double[] {Double.NaN,Double.NaN};
	}

	@Override
	public double[] getPixelSpacing(int i)
	{
		return new double[] {Double.NaN,Double.NaN};
	}

	@Override
	public Set<String> getReferencedSopInstanceUidSet()
	{
		return new HashSet<>();
	}

	@Override
	public int getRowCount()
	{
		return 0;
	}

	@Override
	public String getSeriesDate()
	{
		return "";
	}

	@Override
	public String getSeriesTime()
	{
		return "";
	}

	@Override
	public String getSeriesUid()
	{
		return "";
	}

	@Override
	public double getSliceLocation()
	{
		return Double.NaN;
	}

	@Override
	public double getSliceLocation(int i)
	{
		return Double.NaN;
	}

	@Override
	public String getSopClassUid()
	{
		return "";
	}

	@Override
	public String getStudyDate()
	{
		return "";
	}

	@Override
	public String getStudyTime()
	{
		return "";
	}

	@Override
	public String getStudyUid()
	{
		return "";
	}

	@Override
	public String getUid()
	{
		return uid;
	}

	@Override
	public void setAcquisitionTime(String string)
	{}

	@Override
	public void setColumnCount(int i) throws IllegalArgumentException
	{}

	@Override
	public void setContentTime(String string)
	{}

	@Override
	public void setFrameOfReferenceUid(String string) throws IllegalArgumentException
	{}

	@Override
	public void setImageOrientationPatient(double[] doubles) throws IllegalArgumentException
	{}

	@Override
	public void setImagePositionPatient(double[] doubles) throws IllegalArgumentException
	{}

	@Override
	public void setInstanceNumber(int i)
	{}

	@Override
	public void setModality(String string)
	{}

	@Override
	public void setNumberOfFrames(int i)
	{}

	@Override
	public void setPixelSpacing(double[] doubles)
	{}

	@Override
	public void setRowCount(int i)
	{}

	@Override
	public void setSeriesDate(String string)
	{}

	@Override
	public void setSeriesTime(String string)
	{}

	@Override
	public void setSeriesUid(String string)
	{}

	@Override
	public void setSliceLocation(double d)
	{}

	@Override
	public void setSopClassUid(String string)
	{}

	@Override
	public void setStudyDate(String string)
	{}

	@Override
	public void setStudyTime(String string)
	{}

	@Override
	public void setStudyUid(String string)
	{}

	@Override
	public void setUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.uid = uid;
	}

}
