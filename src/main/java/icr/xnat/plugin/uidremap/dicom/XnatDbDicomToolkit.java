/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.xnat.plugin.uidremap.dicom;

import icr.etherj.StringUtils;
import icr.etherj.Xml;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.SopInstance;
import icr.etherj.dicom.Study;
import icr.xnat.plugin.uidremap.PluginCode;
import icr.xnat.plugin.uidremap.PluginException;
import icr.xnat.plugin.uidremap.utils.PluginUtils;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author jamesd
 */
public class XnatDbDicomToolkit
{
	private final static Logger logger = LoggerFactory.getLogger(
		XnatDbDicomToolkit.class);

	public Study createStudy(XnatImagesessiondataI sessionData)
	{
		Study study = new XnatDbDicomStudy(sessionData);
		for (XnatImagescandataI scanData : sessionData.getScans_scan())
		{
			Series series = createSeries(scanData);
			if (series != null)
			{
				study.addSeries(series);
			}
		}
		return study;
	}
	
	public Series createSeries(XnatImagescandataI scanData)
	{
		Series series = null;
		try
		{
			series = new XnatDbDicomSeries(scanData);
			processCatalog(series, scanData);
		}
		catch (PluginException ex)
		{
			logger.warn("Error creating series from DB", ex);
		}
		return series;
	}

	public SopInstance createSopInstance(String uid, String filename)
	{
		return new XnatDbDicomSopInstance(uid, filename);
	}

	private void processCatalog(Series series, XnatImagescandataI scanData)
		throws PluginException
	{
		Map<String,String> sopInstFileMap = getSopInstanceUidFileMap(scanData);
		for (String uid : sopInstFileMap.keySet())
		{
			SopInstance sopInst = createSopInstance(uid, sopInstFileMap.get(uid));
			series.addSopInstance(sopInst);
		}
	}

	public static Map<String,String> getSopInstanceUidFileMap(
		XnatImagescandataI scanData) throws PluginException
	{
		Map<String,String> sopInstFileMap = new LinkedHashMap<>();
		XnatImagesessiondata sessionData = 
			XnatImagesessiondata.getXnatImagesessiondatasById(
				scanData.getImageSessionId(), null, false);
		Document doc;
		try
		{
			String path = PluginUtils.getScanCatalog(sessionData, scanData);
			logger.debug("Catalog for UIDs: "+path);
			doc = PluginUtils.streamToDoc(new FileInputStream(path));
		}
		catch (IOException ex)
		{
			throw new PluginException("DICOM catalog error",
				PluginCode.HttpUnprocessableEntity, ex);
		}
		Node node = Xml.getFirstMatch(doc.getDocumentElement(), "cat:entries");
		NodeList children = node.getChildNodes();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			if (!child.getNodeName().equals("cat:entry"))
			{
				continue;
			}
			NamedNodeMap attrs = child.getAttributes();
			String uid = null;
			String uri = null;
			Node uidNode = attrs.getNamedItem("UID");
			if (uidNode != null)
			{
				uid = uidNode.getTextContent();
			}
			Node uriNode = attrs.getNamedItem("URI");
			if (uriNode != null)
			{
				uri = uriNode.getTextContent();
			}
			if (!StringUtils.isNullOrEmpty(uid) && !StringUtils.isNullOrEmpty(uri))
			{
				sopInstFileMap.put(uid, uri);
			}
			else
			{
				logger.warn("Scan "+scanData.getId()+
					"'s catalog contains entry with empty UID or URI");
			}
		}
		logger.debug("UIDs found: "+sopInstFileMap.size());
		return sopInstFileMap;
	}
}