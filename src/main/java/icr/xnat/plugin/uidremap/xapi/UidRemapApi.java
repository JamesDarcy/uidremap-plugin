/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.xnat.plugin.uidremap.xapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import icr.etherj.IoUtils;
import icr.etherj.PathScan;
import icr.etherj.StringUtils;
import icr.etherj.Xml;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.dicom.DicomReceiver;
import icr.etherj.dicom.DicomToolkit;
import icr.etherj.dicom.Patient;
import icr.etherj.dicom.PatientRoot;
import icr.etherj.dicom.Series;
import icr.etherj.dicom.SopInstance;
import icr.etherj.dicom.Study;
import icr.xnat.plugin.uidremap.PluginCode;
import icr.xnat.plugin.uidremap.PluginException;
import icr.xnat.plugin.uidremap.Result;
import icr.xnat.plugin.uidremap.Security;
import icr.xnat.plugin.uidremap.dicom.XnatDbDicomToolkit;
import icr.xnat.plugin.uidremap.utils.DisplayUtils;
import icr.xnat.plugin.uidremap.utils.PluginUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.dcm4che2.data.DicomObject;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiProjectRestController;
import org.nrg.xapi.rest.Experiment;
import org.nrg.xapi.rest.Project;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author jamesd
 */
@Api(description="XNAT UID Remap API")
@XapiRestController
@RequestMapping(value="/uidremap")
@JsonIgnoreProperties(value = { "created" })
public class UidRemapApi extends AbstractXapiProjectRestController
{
	private final static Logger logger = LoggerFactory.getLogger(UidRemapApi.class);

	@Autowired
	public UidRemapApi(final UserManagementServiceI userManagementService,
		final RoleHolder roleHolder)
	{
		super(userManagementService, roleHolder);
	}

	@ApiOperation(value="Initiate UID check")
	@ApiResponses({
		@ApiResponse(code=200, message="Command completed"),
		@ApiResponse(code=422, message="Unprocessable request")
	})
	@XapiRequestMapping(
		value="/projects/{projectId}/sessions/{sessionId}",
		method=RequestMethod.POST,
		restrictTo=AccessLevel.Admin)
	@ResponseBody
	public ResponseEntity<String> remapUids(
		@ApiParam(value="Project ID") @PathVariable("projectId") @Project String projectId,
		@ApiParam(value="Session ID") @PathVariable("sessionId") @Experiment String sessionId)
		throws PluginException
	{
		logger.info("UidRefreshUid::checkUids(projectId="+projectId+
			", sessionId="+sessionId+")");
		UserI user = getSessionUser();
		if (logger.isDebugEnabled())
		{
			logger.debug("POST /projects/"+projectId+"/sessions/"+sessionId+
				" by user "+user.getUsername());
		}
		Security.checkProject(user, projectId);
		Security.checkSession(user, sessionId);
		XnatImagesessiondata sessionData = PluginUtils.getImageSessionData(
			sessionId, user);
		Security.checkPermissions(user, sessionData.getXSIType()+"/project", projectId,
			Security.Create, Security.Edit, Security.Read);

		Result result = remapUidsImpl(user, sessionId);

		return new ResponseEntity<>(result.getMessage(), result.getStatus());
	}

	private Map<String,String> compare(Study dbStudy, Study fsStudy)
	{
		Map<String,String> uidFixMap = new HashMap<>();
		String dbStudyUid = dbStudy.getUid();
		String fsStudyUid = fsStudy.getUid();
		if (!dbStudyUid.equals(fsStudyUid))
		{
			uidFixMap.put(dbStudyUid, fsStudyUid);
			logger.warn("Study - DB: "+String.format("%64s", dbStudyUid)+
				"   FS: "+String.format("%64s", fsStudyUid));
		}
		Map<Integer,String> dbSeriesNumUidMap = createSeriesNumUidMap(
			dbStudy.getSeriesList());
		Map<Integer,String> fsSeriesNumUidMap = createSeriesNumUidMap(
			fsStudy.getSeriesList());
		for (Integer num : dbSeriesNumUidMap.keySet())
		{
			Series dbSeries = dbStudy.getSeries(dbSeriesNumUidMap.get(num));
			String dbSeriesUid = dbSeries.getUid();
			Series fsSeries = fsStudy.getSeries(fsSeriesNumUidMap.get(num));
			String fsSeriesUid = fsSeries.getUid();
			if (!dbSeriesUid.equals(fsSeriesUid))
			{
				uidFixMap.put(dbSeriesUid, fsSeriesUid);
				logger.warn("Series - DB: "+String.format("%64s", dbSeriesUid)+
					"   FS: "+String.format("%64s", fsSeriesUid));
			}
			compare(uidFixMap, dbSeries, fsSeries);
		}

		return uidFixMap;
	}

	private void compare(Map<String, String> uidFixMap, Series dbSeries,
		Series fsSeries)
	{
		Map<String,String> dbFileSopInstMap = createFileSopInstMap(dbSeries);
		Map<String,String> fsFileSopInstMap = createFileSopInstMap(fsSeries);
		int preSize = uidFixMap.size();
		for (String file : dbFileSopInstMap.keySet())
		{
			String dbUid = dbFileSopInstMap.get(file);
			String fsUid = fsFileSopInstMap.get(file);
			if (!dbUid.equals(fsUid))
			{
				uidFixMap.put(dbUid, fsUid);
			}
		}
		int delta = uidFixMap.size() - preSize;
		if (delta > 0)
		{
			logger.warn(String.format("%d", delta)+
				" mismatched SOP instance UIDs found");
		}
	}

	private Map<String,String> createFileSopInstMap(Series series)
	{
		Map<String,String> map = new HashMap<>();
		for (SopInstance sopInst : series.getSopInstanceList())
		{
			map.put(sopInst.getFile().getName(), sopInst.getUid());
		}
		return map;
	}

	private Map<Integer,String> createSeriesNumUidMap(List<Series> seriesList)
	{
		Map<Integer,String> map = new TreeMap<>();
		for (Series series : seriesList)
		{
			map.put(series.getNumber(), series.getUid());
		}
		return map;
	}

	private Study parseDbData(XnatImagesessiondata sessionData)
	{
		XnatDbDicomToolkit tk = new XnatDbDicomToolkit();
		Study study = tk.createStudy(sessionData);
		return study;
	}

	private Study parseFileData(XnatImagesessiondata sessionData)
		throws PluginException
	{
		String path = PluginUtils.getExperimentPath(sessionData)+"SCANS"+File.separator;
		DicomToolkit tk = DicomToolkit.getToolkit();
		DicomReceiver rx = new DicomReceiver();
		PathScan<DicomObject> pathScan = tk.createPathScan();
		pathScan.addContext(rx);
		try
		{
			pathScan.scan(path, true);
		}
		catch (IOException ex)
		{
			throw new PluginException(
				"Error parsing filesystem data for session "+sessionData.getId(),
				ex);
		}
		PatientRoot root = rx.getPatientRoot();
		List<Patient> patients = root.getPatientList();
		if (patients.size() != 1)
		{
			throw new PluginException(
				"Expecting a single patient. Found: "+patients.size());
		}
		List<Study> studies = patients.get(0).getStudyList();
		if (studies.size() != 1)
		{
			throw new PluginException(
				"Expecting a single study. Found: "+studies.size());
		}
		return studies.get(0);
	}

	private Document readCatalog(String catalogPath) throws PluginException
	{
		Document doc = null;
		InputStream is = null;
		try
		{
			logger.debug("Catalog for UIDs: "+catalogPath);
			is = new FileInputStream(catalogPath);
			doc = PluginUtils.streamToDoc(is);
		}
		catch (IOException ex)
		{
			throw new PluginException("DICOM catalog error",
				PluginCode.HttpUnprocessableEntity, ex);
		}
		finally
		{
			IoUtils.safeClose(is);
		}
		return doc;
	}

	private void remapScanCatalog(XnatImagesessiondata sessionData,
		XnatImagescandata scanData, Map<String,String> uidFixMap)
		throws PluginException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		DisplayUtils.display(scanData, ps);
		logger.debug(baos.toString());

		String catalogPath = PluginUtils.getScanCatalog(sessionData, scanData);
		Document doc = readCatalog(catalogPath);

		Node node = Xml.getFirstMatch(doc.getDocumentElement(), "cat:entries");
		NodeList children = node.getChildNodes();
		int nUids = 0;
		for (int i=0; i<children.getLength(); i++)
		{
			Node child = children.item(i);
			if (!child.getNodeName().equals("cat:entry"))
			{
				continue;
			}
			NamedNodeMap attrs = child.getAttributes();
			String dbUid;
			Node uidNode = attrs.getNamedItem("UID");
			if (uidNode != null)
			{
				dbUid = uidNode.getTextContent();
				if (StringUtils.isNullOrEmpty(dbUid))
				{
					logger.warn("Scan "+scanData.getId()+
						"'s catalog contains entry with empty UID");
					continue;
				}
			}
			else
			{
				logger.warn("Scan "+scanData.getId()+
					"'s catalog contains entry with no UID attribute");
				continue;
			}
			if (uidFixMap.containsKey(dbUid))
			{
				String fsUid = uidFixMap.get(dbUid);
				if (!StringUtils.isNullOrEmpty(fsUid))
				{
					uidNode.setTextContent(fsUid);
					nUids++;
				}
			}
		}
		if (nUids > 0)
		{
			writeCatalog(doc, catalogPath);
		}
		logger.debug("UIDs remapped: "+nUids);
	}

	private Result remapUidsImpl(UserI user, String sessionId)
		throws PluginException
	{
		XnatImagesessiondata sessionData = PluginUtils.getImageSessionData(
			sessionId, user);

		Study dbStudy = parseDbData(sessionData);
		DisplayUtils.debugDisplayable(logger, "Database: ", dbStudy, true);
		Study fsStudy = parseFileData(sessionData);
		DisplayUtils.debugDisplayable(logger, "Filesystem: ", fsStudy, true);
		Map<String,String> uidFixMap = compare(dbStudy, fsStudy);
		if (uidFixMap.isEmpty())
		{
			return new Result("Nothing to remap", HttpStatus.OK);
		}

		String dbSessionUid = sessionData.getUid();
		if (uidFixMap.containsKey(dbSessionUid))
		{
			String fsSessionUid = uidFixMap.get(dbSessionUid);
			if (!StringUtils.isNullOrEmpty(fsSessionUid))
			{
				sessionData.setUid(fsSessionUid);
				saveSessionData(sessionData, user);
				sessionData = PluginUtils.getImageSessionData(sessionId, user);
			}
			else
			{
				logger.warn(
					"Session UID from filesystem null or empty. Session ID: "+
						sessionId);
			}
		}
		for (XnatImagescandataI scanDataI : sessionData.getScans_scan())
		{
			String dbScanUid = scanDataI.getUid();
			XnatImagescandata scanData = sessionData.getScanById(scanDataI.getId());
			if (uidFixMap.containsKey(dbScanUid))
			{
				String fsScanUid = uidFixMap.get(scanData.getUid());
				if (!StringUtils.isNullOrEmpty(fsScanUid))
				{
					scanData.setUid(fsScanUid);
					saveScanData(scanData, user);
					scanData = PluginUtils.getImageScanData(sessionData, scanData.getId());
				}
				else
				{
					logger.warn(
						"Scan UID from filesystem null or empty. Scan ID: "+
							scanData.getId());
				}
			}
			remapScanCatalog(sessionData, scanData, uidFixMap);
		}

		return new Result("UIDs remapped", HttpStatus.OK);
	}

	private void saveScanData(XnatImagescandata scanData, UserI user)
		throws PluginException
	{
		try
		{
			EventMetaI eventMeta = EventUtils.DEFAULT_EVENT(user,
				"Save image session "+scanData.getId());
			boolean scanDataSaved = SaveItemHelper.authorizedSave(scanData,
				user, false, true, eventMeta);
			if (scanDataSaved)
			{
				logger.info("Image session "+scanData.getId()+" saved");
			}
			else
			{
				logger.error("Image session "+scanData.getId()+" not saved");
			}
		}
		catch (Exception ex)
		{
			String message = "Error saving image session"+
				((scanData != null) ? " "+scanData.getId() : "");
			throw new PluginException(message, PluginCode.HttpInternalError, ex);
		}
	}

	private void saveSessionData(XnatImagesessiondata sessionData, UserI user)
		throws PluginException
	{
		try
		{
			EventMetaI eventMeta = EventUtils.DEFAULT_EVENT(user,
				"Save image session "+sessionData.getId());
			boolean sessionDataSaved = SaveItemHelper.authorizedSave(sessionData,
				user, false, true, eventMeta);
			if (sessionDataSaved)
			{
				logger.info("Image scan "+sessionData.getId()+" saved");
			}
			else
			{
				logger.error("Image scan "+sessionData.getId()+" not saved");
			}
		}
		catch (Exception ex)
		{
			String message = "Error saving image session"+
				((sessionData != null) ? " "+sessionData.getId() : "");
			throw new PluginException(message, PluginCode.HttpInternalError, ex);
		}
	}

	private void writeCatalog(Document doc, String catalogPath)
	{
		OutputStream os = null;
		try
		{
			os = new FileOutputStream(catalogPath);
			StreamResult result = new StreamResult(
				new BufferedWriter(new OutputStreamWriter(os)));
			TransformerFactory transformerFactory =
				TransformerFactory.newInstance();
			try
			{
				transformerFactory.setAttribute("indent-number", 3);
			}
			catch (IllegalArgumentException ex)
			{
				// MATLAB uses Saxon 6.5 instead of the standard JDK Xalan
				logger.warn("Attribute 'indent-number' not supported by TransformerFactory: "+
					transformerFactory.getClass().getCanonicalName());
			}
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
		}
		catch (IOException | TransformerException ex)
		{
			logger.warn("Catalog write failed", ex);
		}
		finally
		{
			IoUtils.safeClose(os);
		}
	
	}

}
